/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

/**
 *
 * @author nati2
 */
public class Jugador {
    
     private String name;
     private String gameDate;
     private int movements;
     private int leaps;
    private String gameType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGameDate() {
        return gameDate;
    }

    public void setGameDate(String gameDate) {
        this.gameDate = gameDate;
    }

    public int getMovements() {
        return movements;
    }

    public void setMovements(int movements) {
        this.movements = movements;
    }

    public int getLeaps() {
        return leaps;
    }

    public void setLeaps(int leaps) {
        this.leaps = leaps;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }
    
    public String getPlayerDetails() {
        String temp = "%s,%s,%s,%s,%s";
        return String.format(temp, getName(), getGameDate(), getMovements(), getLeaps(), getGameType());
    }
    
}
